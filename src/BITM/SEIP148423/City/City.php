<?php

namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class City extends DB{

    public $id="";
    public $name="";
    public $city="";

    public function __construct()
    {
        parent::__construct();
    }

    /*
    public function index(){

        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->city."<br>";
    }
*/

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }

        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('city',$data)){
            $this->city=$data['city'];
        }

    }

    public function store(){
        $arrData=array($this->name, $this->city);
        $sql="INSERT INTO city (name,city) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('create.php'); // redirect korte hobe create.php te tai utility.php use korechi //

    }//end of store method

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from city');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from city where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of index();

}

//$objCity = new City();

?>